# Resources

A general list of resources and links relating to the Sipeed Longan Nano RISCV board. TODO: organization/catergorization.

- [PlatformIO IDE plugin for VS Code](https://marketplace.visualstudio.com/items?itemName=platformio.platformio-ide)
- [Fork of DFU Utils for GD32 boards](https://github.com/riscv-mcu/gd32-dfu-utils)
- [udev rules for PlatformIO boards](https://docs.platformio.org/en/latest/faq.html#platformio-udev-rules) (or [here](https://raw.githubusercontent.com/platformio/platformio-core/master/scripts/99-platformio-udev.rules) for direct link)
- [How to play custom videos using the demo program](https://www.mikrozone.sk/pluginy/content/content.php?content.148)
- [Blog post about longan nano](https://www.susa.net/wordpress/2019/10/longan-nano-gd32vf103/)
- [Bitmap conversion tools](http://dl.sipeed.com/LONGAN/Nano/Firmware/badapple_demo_tools/tools_bmp2hex.zip)
- [Video playing demo program](https://github.com/sipeed/Longan_GD32VF_examples)
- [Blinking LED example](https://github.com/sipeed/platform-gd32v/tree/master/examples/longan-nano-blink)
