# longan-nano-docs
Documentation for the Longan Nano device.

# Setup (Linux/macOS)

## Building firmware binaries

1. Download the [PlatformIO IDE plugin](https://marketplace.visualstudio.com/items?itemName=platformio.platformio-ide) for VS Code.
2. Open a PlatformIO project folder in VS Code, ensuring that there is a platformio.ini file at the root.
3. Type `pio run` in a VS Code terminal to build the project. 
4. The `firmware.bin` file needed for upload to the device will be found in `.pio/build/sipeed-longan-nano/`.

## Uploading to the device 

### Manually (using dfu-util)

1. Download [this](https://github.com/riscv-mcu/gd32-dfu-utils) fork of the dfu-utils program for GigaDevice boards.
2. Run `./configure`, `make`, then `sudo make install` to build the dfu-utils. Take note of where they are installed, in case you already have the standard version installed through your package manager.
3. Put your device in DFU mode by holding the power button, and quickly pressing the reset button. The screen should turn black and you should see info in lsusb/dmesg (device id `28e9:0189`).
4. Ensuring that `dfu-util` points to the GD32 fork, run `dfu-util -d 28e9:0189 -a 0 --dfuse-address 0x08000000:leave -D firmware.bin` as root, where firmware.bin corresponds to a compiled firmware file.
5. (Optional) If you want to be able to upload to the board as a non-root user, follow [this guide](https://docs.platformio.org/en/latest/faq.html#platformio-udev-rules) for installing udev rules.

### Using VS Code/PlatformIO

1. Put your device in DFU mode by holding the power button, and quickly pressing the reset button. The screen should turn black and you should see info in lsusb/dmesg (device id `28e9:0189`).
2. Ensure that the `platformio.ini` file at the root of your project has `upload_protocol` set to `dfu`. For example,

```
[env:sipeed-longan-nano]
platform = gd32v
board = sipeed-longan-nano
framework = gd32vf103-sdk
upload_protocol = dfu
```
3. Run `pio run --target upload` in a VS Code terminal. Note that if you want to run this without being root, you need the [udev rules](https://docs.platformio.org/en/latest/faq.html#platformio-udev-rules).


# Setup (Windows)

## Building firmware binaries

Same as [for Linux/macOS](#Building-firmware-binaries).

## Uploading to the device

1. Download [this](http://dl.sipeed.com/LONGAN/Nano/Tools/GD32_MCU_Dfu_Tool_V3.8.1.5784_1.rar) driver/tool package from the Sipeed website.
2. Extract the archive and install the drivers in the GD32 MCU DFU Drivers directory.
3. Put your device in DFU mode by holding the power button, and quickly pressing the reset button. The screen should turn black and you should see info in lsusb/dmesg (device id `28e9:0189`).
4. Launch the flasher tool in the tools directory. Your board should appear as the default in the DFU Device dropdown if it is in DFU mode.
5. Ensure that Download to Device is selected, and that the address dropdown is set to `8000000`. Press Leave DFU.

# Resources 

See [resources.md](resources.md).